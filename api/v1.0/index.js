var express = require('express');
var router = express.Router();

const userRouter = require('./modules/user/routes');
const postRouter = require('./modules/post/routes');

router.get('/', function (req, res, next) {
  res.send('Hello v1.0 GET API from Kutumb');
});

router.post('/', function (req, res, next) {
  res.send('Hello v1.0 POST API from Kutumb');
});

router.use('/user', userRouter);
router.use('/post', postRouter)

module.exports = router;
