const object = require('./post');
const functions = require('../../../../common/functions');

const controller = {
  // API for count of views on a post
  viewsOfPost: async (req, res, next) => {
    try {
      const result = await object
        .postService()
        .viewsOfPost(res.locals.requestedData);
      res.send(
        functions.responseGenerator(
          result.statusCode,
          result.message,
          result.data
        )
      );
    } catch (error) {
      return next(error);
    }
  },

  // Viewer's List of a post/article
  viewersOfPost: async (req, res, next) => {
    try {
      const result = await object
        .postService()
        .viewersOfPost(res.locals.requestedData);
      res.send(
        functions.responseGenerator(
          result.statusCode,
          result.message,
          result.data
        )
      );
    } catch (error) {
      return next(error);
    }
  },

    // post/article viewed: insert into view[DB]
    postViewed: async (req, res, next) => {
      try {
        const result = await object
          .postService()
          .postViewed(res.locals.requestedData);
        res.send(
          functions.responseGenerator(
            result.statusCode,
            result.message,
            result.data
          )
        );
      } catch (error) {
        return next(error);
      }
    },
};

module.exports = controller;
