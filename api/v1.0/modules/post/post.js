const functions = require('../../../../common/functions');
const config = require('../../../../config');
const validator = require('validator');
const statusCode = require('../../../../common/statusCode');
const message = require('../../../../common/message');
const fs = require('fs');
const db = require(`./database/${config.database}/${config.database}`);

class PostService {
  /**
   * API to get count of views of a post/article
   * @param {*} req (post details)
   * @param {*} res (json with success/failure)
   */
  async viewsOfPost(info) {
    try {
      const { postId } = info;

      if (!postId) {
        throw {
          statusCode: statusCode.bad_request,
          message: message.badRequest,
          data: null,
        };
      }

      const viewsOfPost = await db.postDatabase().getViewsofPost(postId);

      if (!viewsOfPost.length) {
        throw {
          statusCode: statusCode.bad_request,
          message: message.invalidDetails,
          data: null,
        };
      }

      const PostViews = {
        fullName: viewsOfPost[0].fullName,
      };

      return {
        statusCode: statusCode.success,
        message: message.success,
        data: PostViews,
      };
    } catch (error) {
      throw {
        statusCode: error.statusCode,
        message: error.message,
        data: JSON.stringify(error),
      };
    }
  }

  /**
   * API to get list of post/article viewers.
   * @param {*} req (post details)
   * @param {*} res (json with success/failure)
   */
  async viewersOfPost(info) {
    try {
      const { postId } = info;

      if (!postId) {
        throw {
          statusCode: statusCode.bad_request,
          message: message.badRequest,
          data: null,
        };
      }

      const ListOfViewers = await db.postDatabase().getListOfViewers(postId);

      if (!ListOfViewers.length) {
        throw {
          statusCode: statusCode.bad_request,
          message: message.invalidDetails,
          data: null,
        };
      }

      const ListOfViewersObj = {
        ListOfViewersObj,
      };

      return {
        statusCode: statusCode.success,
        message: message.success,
        data: ListOfViewersObj,
      };
    } catch (error) {
      throw {
        statusCode: error.statusCode,
        message: error.message,
        data: JSON.stringify(error),
      };
    }
  }

  /**
   * API to insert view of post/article by viewer.
   * @param {*} req (post details)
   * @param {*} res (json with success/failure)
   */
  async postViewed(info) {
    try {
      const { postId } = info;

      if (!postId) {
        throw {
          statusCode: statusCode.bad_request,
          message: message.badRequest,
          data: null,
        };
      }

      const postViewed = await db.postDatabase().postViewed(info);

      return {
        statusCode: statusCode.success,
        message: message.success,
        data: postViewed,
      };
    } catch (error) {
      throw {
        statusCode: error.statusCode,
        message: error.message,
        data: JSON.stringify(error),
      };
    }
  }
}

module.exports = {
  postService: function () {
    return new PostService();
  },
};
