const con = require('../../../../../../common/database/mysql');
const util = require('util');
const query = util.promisify(con.query).bind(con);
const { databaseInitial } = require('../../../../../../config');
const { connection_failed } = require('../../../../../../common/statusCode');

class PostDatabase {
  /**
   * Database call to get view count of a post/article
   * @param {*} req (postId)
   * @param {*} res (json with success/failure)
   */
  async getViewsofPost(postId) {
    try {
      const sqlSelectQuery = `SELECT COUNT(Id) as PostViews FROM ${databaseInitial}Post WHERE PostId = ?`;
      const details = await query(sqlSelectQuery, [postId]);
      return details;
    } catch (error) {
      throw {
        statusCode: connection_failed,
        message: error.message,
        data: JSON.stringify(error),
      };
    }
  }

  /**
   * Database call to get view count of a post/article
   * @param {*} req (postId)
   * @param {*} res (json with success/failure)
   */
  async getListOfViewers(postId) {
    try {
      const sqlSelectQuery = `SELECT u.Id as viewerId, u.Name as viewerName, u.address as viewerAddress
                                FROM ${databaseInitial}user u
                                INNER JOIN ${databaseInitial}view v
                                ON u.Id = v.userId WHERE v.postId = PostId`;
      const details = await query(sqlSelectQuery, [postId]);
      return details;
    } catch (error) {
      throw {
        statusCode: connection_failed,
        message: error.message,
        data: JSON.stringify(error),
      };
    }
  }

  /**
   * Database call for inserting view information
   * @param {*} req (view details)
   * @param {*} res (json with success/failure)
   */
  async postViewed(info) {
    try {
      const sqlInsertQuery = `INSERT INTO ${databaseInitial}view (userId, postId) VALUES (?, ?)`;
      const details = await query(sqlInsertQuery, [
        info.userId,
        info.postId,
      ]);
      return details;
    } catch (error) {
      throw {
        statusCode: connection_failed,
        message: error.message,
        data: JSON.stringify(error),
      };
    }
  }
}

module.exports = {
  postDatabase: function () {
    return new PostDatabase();
  },
};
