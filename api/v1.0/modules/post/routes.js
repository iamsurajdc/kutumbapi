const router = require('express').Router();
const api = require('./controller');
const auth = require('../../../../common/authentication');

// Middle layer for POST(article) API
router.get('/viewsOfPost', auth.decryptRequest, api.viewsOfPost);
router.get('/viewersOfPost', auth.decryptRequest, api.viewersOfPost);
router.post('/postViewed', auth.decryptRequest, api.postViewed);

module.exports = router;
